---
title: Haskell for madmen

---
_If you spot a mistake, grammar error or unclear passage, please please open an
issue or make a merge request over on my [gitlab repository](https://gitlab.com/DrBearhands/blog)
and help me make this tutorial better._

# Introduction

There appears to be some interest in a Haskell tutorial that does not take
things slowly, but rather dives right into creating a real-world application,
specifically a webserver. In order to do this though, we will need to use monads
right from the get-go, and monads are traditionally considered hard. When I
first learned Haskell myself, the way of handling this was "just ignore it and
trust me when I say it works and is important". This merely confused me, so I
don't want to do this, and would rather explain also the *why*, not just the
*how*. This requires some non-trivial mathematics. Furthermore, we will also
need to take an early dive into project setup and the Haskell ecosystem, two
aspects that are harder than they have any right to be.

And so this tutorial is born: Haskell for madmen.


# Index

* [Setup](1.setup)
* [Hello, monad!](2.hello-monad)

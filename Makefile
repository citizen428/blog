PANDOC=pandoc
PFLAGS=-t html5 -r markdown --mathml -s -A "src/style/pandoc.html"
SRC_DIR=src
OUT_DIR=public
UPLOAD=upload
SRC_MD_FILES=$(filter-out $(SRC_DIR)/index.md, $(shell find $(SRC_DIR)/ -type f -name "*.md") )
OUT_MD_FILES=$(patsubst $(SRC_DIR)/%.md,$(OUT_DIR)/%/index.html,$(SRC_MD_FILES))
UPLOAD_FILES=$(patsubst $(SRC_DIR)/%.md,$(UPLOAD)/%,$(SRC_MD_FILES))
SRC_CSS=$(shell find $(SRC_DIR)/ -type f -name '*.css')
OUT_CSS=$(patsubst $(SRC_DIR)/%.css,$(OUT_DIR)/%.css,$(SRC_CSS))

.phony: upload all $(UPLOAD)/%

all: $(OUT_MD_FILES) $(OUT_CSS) $(OUT_DIR)/index.html

upload/%: # $(SRC_DIR)/% upload_deps
	#if [[ $(HASKELL_POSTS) == *"haskell"*]]; then echo "foo"; fi
	echo $(HASKELL_POSTS)

upload: $(UPLOAD_FILES)


HASKELL_POSTS=$(shell curl https://dev.to/api/articles?username=drbearhands \
	| grep -Po '"canonical_url": *\K"[^"]*"' \
	| grep "haskell" \
	| grep -oP 'https://dev.to/drbearhands/\K[^"]*' )


$(OUT_DIR)/index.html: $(SRC_DIR)/index.md
	mkdir -p $(shell dirname $@)
	$(PANDOC) $(PFLAGS) -o $@ $<

$(OUT_DIR)/%/index.html: $(SRC_DIR)/%.md
	mkdir -p $(shell dirname $@)
	$(PANDOC) $(PFLAGS) -o $@ $<

$(OUT_DIR)/%.css: $(SRC_DIR)/%.css
	mkdir -p $(shell dirname $@)
	cp $< $@
